ip:=$(shell kubectl get svc ${HELM_RELEASE}-traefik --namespace=${HELM_NAMESPACE} -o go-template='{{(index .status.loadBalancer.ingress 0).ip}}')
CF_RECORD_ID:=$(shell curl -s -X GET "https://api.cloudflare.com/client/v4/zones/${CF_ZONE_ID}/dns_records?name=*.${CI_ENVIRONMENT_SLUG}.${DEPLOYMENT_DOMAIN_NAME}" \
		-H "X-Auth-Email: ${CF_AUTH_EMAIL}" \
		-H "Authorization: Bearer ${CF_DNS_API_TOKEN}" \
		-H "Content-Type: application/json" | jq --raw-output '.result[0].id')

cloudflare-record-create:
	curl -s -X POST "https://api.cloudflare.com/client/v4/zones/${CF_ZONE_ID}/dns_records" \
        -H "X-Auth-Email: ${CF_AUTH_EMAIL}" \
        -H "Authorization: Bearer ${CF_DNS_API_TOKEN}" \
        -H "Content-Type: application/json" \
        --data '{"type":"A","name":"*.${CI_ENVIRONMENT_SLUG}.${DEPLOYMENT_DOMAIN_NAME}","content":"${ip}","ttl":1,"proxied":false}'

cloudflare-record-edit:
	curl -s -X PUT "https://api.cloudflare.com/client/v4/zones/${CF_ZONE_ID}/dns_records/${CF_RECORD_ID}" \
        -H "X-Auth-Email: ${CF_AUTH_EMAIL}" \
        -H "Authorization: Bearer ${CF_DNS_API_TOKEN}" \
        -H "Content-Type: application/json" \
		--data '{"type":"A","name":"*.${CI_ENVIRONMENT_SLUG}.${DEPLOYMENT_DOMAIN_NAME}","content":"${ip}","ttl":1}'
